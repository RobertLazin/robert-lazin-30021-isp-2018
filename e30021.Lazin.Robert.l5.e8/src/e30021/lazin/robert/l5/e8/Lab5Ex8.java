package e30021.lazin.robert.l5.e8;
import java.util.ArrayList;
public class Lab5Ex8 {

    public static void main(String[] args) {
        Button b = new Button();
        ArrayList<Button> nrButtons = new ArrayList<Button>();
        for (int i = 0; i < 15; i++) {
            nrButtons.add(b);
            if (nrButtons.size() > 15) {
                System.exit(0);
            }

        }
        Phone p = new Phone(nrButtons);
        p.print();

    }

}
