        
package e30021.lazin.robert.l5.e6;


public class Sensor {
    private int value;
    private String location="Car";

    public int getValue(){
        return value;
    }
    public void  setValue(int q){
        this.value=q;
    }
    public void displaySensorInfo(){
        System.out.println(" Value= "+value+" Location= "+location);
    }
}