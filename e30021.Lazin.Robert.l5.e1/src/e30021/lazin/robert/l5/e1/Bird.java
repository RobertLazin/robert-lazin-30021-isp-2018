package e30021.lazin.robert.l5.e1;

class Bird {

    public void move() {
        System.out.println("The bird is moving.");
    }
}

class Penguin extends Bird {

    public void move() {
        System.out.println("The PENGUIN is swiming.");
    }
}

class Parrot extends Bird {

    public void move() {
        System.out.println("The Parrot is talking");
    }

}

class Goose extends Bird {

    public void move() {
        System.out.println("The GOOSE is flying.");
    }
}
