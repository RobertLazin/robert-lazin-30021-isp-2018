package e30021.lazin.robert.l5.e1;

public class BirdController {

    Bird[] birds = new Bird[4];

    BirdController() {
        birds[0] = createBird();
        birds[1] = createBird();
        birds[2] = createBird();
        birds[3] = createBird();
    }

    public void relocateBirds() {
        for (int i = 0; i < birds.length; i++) {
            birds[i].move();
        }
    }

    private Bird createBird() {
        int i = (int) (Math.random() * 15);
        if (i < 6) {
            return new Penguin();
        } else if (i > 5 && i < 11) {
            return new Goose();
        } else {
            return new Parrot();
        }
    }

}
