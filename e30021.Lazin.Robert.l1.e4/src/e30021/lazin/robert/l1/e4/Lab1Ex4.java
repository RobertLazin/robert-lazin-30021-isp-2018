package e30021.lazin.robert.l1.e4;

import java.util.*;

public class Lab1Ex4 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int N = scan.nextInt();
        Vector v = new Vector(N);
        Random rand = new Random();
        for (int i = 0; i < N; i++) {
            v.add(rand.nextInt(20) + 1);
        }

        Enumeration vEnum = v.elements();
        System.out.println("\nElements in vector:");

        while (vEnum.hasMoreElements()) {
            System.out.print(vEnum.nextElement() + " ");
        }
        System.out.println();

        int max = 0;
       
        for (int i = 0; i < N; i++) {
            if ((Integer) v.get(i) > max) {
                
                max = (Integer) v.get(i);
            }
        }

        System.out.println(max);

    }

}
