package e30021.lazin.robert.l1.e6;

import java.util.Scanner;

public class Lab1Ex6 {

    //recursive
    static long factor(int N) {

        return (N == 0 ? 1 : N * factor(N - 1));
    }

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        int N = scan.nextInt();

        //non recursive method
        long sum = 1;
        if (N > 0) {

            for (int i = 1; i <= N; i++) {
                sum *= i;
            }
        }
        System.out.println("non recursive result: " + sum);

        System.out.println("recursive result: " + factor(N));

    }

}
