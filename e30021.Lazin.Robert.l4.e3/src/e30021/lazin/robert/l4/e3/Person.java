package e30021.lazin.robert.l4.e3;

public class Person {

    String fullName;
    int age;

    public Person(int age, String fullName) {
        this.age = age;
        this.fullName = fullName;
    }

    public Person() {
        fullName = "Peter";
        age = 31;
    }

    int getAge() {
        return this.age;
    }

    String getFullName() {
        return this.fullName;
    }

    public String toString() {
        return "The person's name is: " + this.fullName + " and its age is: " + getAge();

    }
}
