
package e30021.lazin.robert.l4.e3;


public class Student extends Person{
       int studentNumber;
    public Student(int num, String fullName,int age){
        super(age,fullName);
        studentNumber=num;
    }
    int getStudentNumber(){
        return studentNumber;
    }
    public String toString(){
        return "The person is a student and his name is: "+getFullName()+" and its age is: "+getAge()+"and its number is: "+getStudentNumber();
    }
 
}
