package e30021.lazin.robert.l4.e3;

public class L4Ex3 {

    public static void main(String[] args) {
        Person q = new Person();
        System.out.println(q.toString());
        Person n = new Professor(4, "Doe", 32);
        System.out.println(n.toString());
        Person m = new Student(9, "John", 20);
        System.out.println(m.toString());

    }

}
