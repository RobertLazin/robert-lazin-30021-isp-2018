
package e30021.lazin.robert.l5.e2;


abstract class Bird {
    public abstract void move();
}

    class Penguin extends Bird {
        public void move(){
            System.out.println("The PENGUIN is swiming.");
        }
    }

    class Goose extends Bird {
        public void move(){
            System.out.println("The GOOSE is flying.");
        }
    }
