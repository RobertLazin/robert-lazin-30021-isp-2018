package e30021.lazin.robert.l3.e1;


public class Lab3Ex1 {


    public static void main(String[] args) {
 Vehicle v1=new Vehicle();
        System.out.println("The model is: "+v1.getModel()+" the speed is:"+v1.getSpeed()+" and the fueltype is: "+v1.getFuelType());
        Vehicle v2=new Vehicle("Volvo",120,"Benzin");
        System.out.println(v2);
        for(int i=0;i<5;i++){
            v2.accelerate();
            if (i==2)
                v2.brake();
        }
        System.out.println(v2);
        Vehicle v3=new Vehicle("Toyota",140,"Hybrid");
        System.out.println(v3);
        do{
            v3.brake();
        }while(v3.speed!=50);
        System.out.println(v3);
    }

}

    
    

