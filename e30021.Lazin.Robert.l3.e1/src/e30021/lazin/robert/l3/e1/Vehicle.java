package e30021.lazin.robert.l3.e1;

public class Vehicle {

    String model;
    int speed;
    String fuelType;

    Vehicle() {
        model = "FORD";
        speed = 6;
        fuelType = "Diesel";
    }

    Vehicle(String m, int s, String f) {
        model = m;
        speed = s;
        fuelType = f;
    }

    public String getModel() {
        return model;

    }

    public String getFuelType() {
        return fuelType;
    }

    public int getSpeed() {
        return speed;
    }

    public void accelerate() {
        speed += 5;
    }

    public void brake() {
        speed -= 3;
    }

    public String toString() {
        return "the car's model is:" + model + " and its speed = " + speed + "and its fueltype is" + fuelType;
    }

}
