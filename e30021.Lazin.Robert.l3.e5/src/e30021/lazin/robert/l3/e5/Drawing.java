package e30021.lazin.robert.l3.e5;

public class Drawing {

    public String shap;

    Drawing() {
        shap = "rombus";
    }

    Drawing(String s) {
        shap = s;
    }

    public String drawShapes() {
        return shap;
    }

}
