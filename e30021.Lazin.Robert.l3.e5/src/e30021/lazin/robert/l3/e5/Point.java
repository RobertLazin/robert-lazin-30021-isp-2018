package e30021.lazin.robert.l3.e5;

public class Point {

    double x;
    double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public String toString() {
        return " the x: " + x + " the y: " + y;

    }
}
