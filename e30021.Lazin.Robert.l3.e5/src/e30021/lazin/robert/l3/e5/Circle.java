package e30021.lazin.robert.l3.e5;

public class Circle {

    public double radius;
    public String color;

    Circle() {
        radius = 1.0;
        color = "red";
    }

    Circle(double r, String c) {
        radius = r;
        color = c;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return radius * radius * Math.PI;
    }

}
