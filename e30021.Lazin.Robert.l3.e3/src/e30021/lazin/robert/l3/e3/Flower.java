package e30021.lazin.robert.l3.e3;

public class Flower {

    static int count = 0;

    Flower() {
        count++;
        System.out.println("Flower has been created!");

    }

    public static int getCount() {
        return count;
    }

}
