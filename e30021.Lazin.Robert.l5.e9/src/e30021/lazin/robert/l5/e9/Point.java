
package e30021.lazin.robert.l5.e9;


import java.awt.*;
public class Point extends Shape {
    private int x;
    private int y;
    public Point(Color color,int x,int y){
        super(color);
        this.x=x;
        this.y=y;

    }
    public void draw(Graphics g){
        System.out.println("Drawing a point at x "+x+"and y "+y+""+getColor());
        g.setColor(getColor());
        g.drawLine(0,0,38,38);


    }
}
