
package e30021.lazin.robert.l5.e3;


class Bird implements beast {
    public void move(){
        System.out.println("The bird is moving.");
    }

    @Override
    public void relocateBirds() {

    }

    @Override
    public Bird createBird() {
        return null;
    }
}

class Penguin extends Bird implements beast {
    public void move(){
        System.out.println("The PENGUIN is swiming.");
    }
}

class Goose extends Bird implements beast {
    public void move(){
        System.out.println("The GOOSE is flying.");
    }
}

