package e30021.lazin.robert.l1.e7;

import java.util.*;

public class Lab1Ex7 {

    public static void main(String[] args) {

        System.out.println("Guess the number: ");
        Random rand = new Random();
        int randomNumber = rand.nextInt(50);
        Scanner scan = new Scanner(System.in);

        int yourNumber;

        int retries;
        for (retries = 1;; retries++) {

            yourNumber = scan.nextInt();

            if (yourNumber == randomNumber) {
                System.out.println("You've guessed the number: " + randomNumber);
                Runtime.getRuntime().exit(0);
            }

            if (retries >= 3) {
                System.out.println("You lost");
                Runtime.getRuntime().exit(0);
            } else if (yourNumber > randomNumber) {
                System.out.println("Wrong answer, your number is too high");
            } else if (yourNumber < randomNumber) {
                System.out.println("Wrong answer, your number is too low");

            }
        }

    }

}
