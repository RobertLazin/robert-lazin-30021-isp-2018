
package e30021.lazin.robert.l5.e5;


class Student extends Person{
    String uni;
    Student(String name, String uni){
        super(name);
        this.uni = uni;
    }
 
    void display(){
        System.out.println("Student "+name+" universitatea "+uni);
    }
}
