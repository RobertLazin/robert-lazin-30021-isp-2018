
package e30021.lazin.robert.l5.e5;


class Employee extends Student{
    int salary;
    Employee(String name,String uni,int salary){
        super(name,uni);
        this.salary = salary;
    }
    void display(){
        System.out.println("Student "+name+" Salar "+salary);
    }
}
