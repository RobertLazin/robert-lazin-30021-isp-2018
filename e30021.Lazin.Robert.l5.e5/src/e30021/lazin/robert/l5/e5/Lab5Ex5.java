package e30021.lazin.robert.l5.e5;

public class Lab5Ex5 {

    public static void main(String[] args) {
        Person p = new Person("Dani");
        Student s = new Student("Mihai", "UTCN");
        Employee q = new Employee("Rares", "UTCN", 6000);
        p.display();
        s.display();
        q.display();
    }

}
