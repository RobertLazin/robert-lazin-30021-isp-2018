package e30021.lazin.robert.l1.e5;

import java.util.*;

public class Lab1Ex5 {

    public static void main(String[] args) {
        Random rand = new Random();
        Vector<Integer> v = new Vector<>(10);
        
        for (int i=0;i<v.capacity() ; i++) {
            v.add(rand.nextInt(30)+1);
        }
       
        
        int n = v.capacity();  
        int aux = 0;  
         for(int i=0; i < n; i++){  
                 for(int j=1; j < (n-i); j++){  
                          if( v.get(j-1) > v.get(j)){  
                                  
                                 aux = v.get(j-1);  
                                 v.set(j-1, v.get(j));  
                                 v.set(j, aux);
                                 
                                 
                         }  
                          
                 }  
         }  
        
        
       for (int i = 0; i < v.size(); i++) {
            int value = v.get(i);
            System.out.print(value+ " ");
        }
        
    }
    
}
