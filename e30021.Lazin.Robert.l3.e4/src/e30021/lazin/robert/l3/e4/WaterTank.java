package e30021.lazin.robert.l3.e4;

public class WaterTank {

    WaterTank() {
        System.out.println("New water tank created.");
    }

    int getIngredient() {
        return (int) (Math.random() * 40);
    }
}
