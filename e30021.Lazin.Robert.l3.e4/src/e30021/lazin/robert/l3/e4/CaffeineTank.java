package e30021.lazin.robert.l3.e4;

class CaffeineTank {

    CaffeineTank() {
        System.out.println("New coffeine tank created.");
    }

    int getIngredient() {
        return (int) (Math.random() * 10);
    }
}
