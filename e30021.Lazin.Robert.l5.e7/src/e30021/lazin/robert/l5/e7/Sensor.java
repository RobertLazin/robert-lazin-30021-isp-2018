
package e30021.lazin.robert.l5.e7;


public class Sensor {
    private int value;
    private String name;

    public Sensor(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return " " +
                "Value= " + value +
                ", Name= " + name +
                '}';
    }
}

