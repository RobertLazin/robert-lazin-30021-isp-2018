
package e30021.lazin.robert.l5.e7;


public class Controller {
    private String location;
    private Sensor tempSensor;

    public Controller(String location, Sensor tempSensor) {
        this.location = location;
        this.tempSensor = tempSensor;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Sensor getTempSensor() {
        return tempSensor;
    }

    public void setTempSensor(Sensor tempSensor) {
        this.tempSensor = tempSensor;
    }
    public void checkTemperature(){
        System.out.println("Temperature= "+tempSensor);
    }

    @Override
    public String toString() {
        return " " +
                "Location= " + location +
                ", TempSensor= " + tempSensor;
    }
}