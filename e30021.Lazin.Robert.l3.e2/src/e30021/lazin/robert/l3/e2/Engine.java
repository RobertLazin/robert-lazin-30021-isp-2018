package e30021.lazin.robert.l3.e2;

public class Engine {

    String fuellType;
    long capacity;
    boolean active;
    int speed;

    Engine(int capacity, boolean active, int speed) {
        this.capacity = capacity;
        this.active = active;
        this.speed = speed;
    }

    Engine(int capacity, boolean active, int speed, String fuellType) {
        this(capacity, active, speed);
        this.fuellType = fuellType;
    }

    Engine() {
        this(2000, false, 40, "Diesel");
    }

    void print() {
        System.out.println("Engine: capacity=" + this.capacity + " fuel=" + fuellType + " active=" + active + " speed=" + speed);
    }

}
