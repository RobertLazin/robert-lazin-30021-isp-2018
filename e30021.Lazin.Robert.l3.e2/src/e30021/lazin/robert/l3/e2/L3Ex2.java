package e30021.lazin.robert.l3.e2;

public class L3Ex2 {

    public static void main(String[] args) {
        Engine tdi = new Engine();
        Engine i16 = new Engine(1600, false, 30, "petrol");
        Engine d30 = new Engine(3000, true, 20, "diesel");
        tdi.print();
        i16.print();
        d30.print();
    }

}
