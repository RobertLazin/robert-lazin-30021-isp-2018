package e30021.lazin.robert.l1.e1;

import java.util.Scanner;

public class Lab1Ex1 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Introduceti primul numar:");
        int n1 = s.nextInt();
        System.out.println("Introduceti al doilea numar:");
        int n2 = s.nextInt();

        if (n1 > n2) {
            System.out.println("n1=" + n1 + " Este mai mare decat n2=" + n2);
        } else if (n2 > n1) {
            System.out.println("n2=" + n2 + " Este mai mare decat n1=" + n1);
        } else {
            System.out.println("n1=" + n1 + " n2=" + n2 + " Sunt egale");
        }
    }

}
